package com.techuniversity.prod.productos;

import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

@Repository //Anotación repositorio spring
public interface ProductoRepository extends MongoRepository<ProductoModel, String> {

}
